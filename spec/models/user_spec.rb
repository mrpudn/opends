# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User do
  subject(:user) { build(:user) }

  let(:email) { user.email }
  let(:password) { user.password }

  describe '.authenticate' do
    let(:incorrect_password) { 'incorrect' }

    context 'with existing user and correct credentials' do
      it 'returns the user' do
        user.save
        expect(described_class.authenticate(email, password)).to eq(user)
      end
    end

    context 'with existing user and incorrect password' do
      it 'returns nil' do
        user.save
        expect(described_class.authenticate(email, incorrect_password)).to be_nil
      end
    end

    context 'with missing user' do
      it 'returns nil' do
        expect(described_class.authenticate(email, password)).to be_nil
      end
    end
  end

  describe '#valid?' do
    let(:upcased_email) { email.upcase }
    let(:incorrect_password) { 'incorrect' }
    let(:short_password) { '1234567' }

    context 'with valid attributes' do
      it { is_expected.to be_valid }
    end

    context 'with missing email' do
      subject { build(:user, email: nil) }

      it { is_expected.not_to be_valid }
    end

    context 'with duplicate email' do
      before { create(:user, email: upcased_email) }

      it { is_expected.not_to be_valid }
    end

    context 'with missing password' do
      subject { build(:user, password: nil) }

      it { is_expected.not_to be_valid }
    end

    context 'with incorrect password confirmation' do
      subject { build(:user, password_confirmation: incorrect_password) }

      it { is_expected.not_to be_valid }
    end

    context 'with short password' do
      subject { build(:user, password: short_password) }

      it { is_expected.not_to be_valid }
    end
  end

  describe '#save' do
    context 'with email that is not downcased' do
      subject(:existing_user) { create(:user, email: upcased_email) }

      let(:upcased_email) { email.upcase }
      let(:downcased_email) { email.downcase }

      after { existing_user.destroy }

      it 'downcases email' do
        expect(existing_user.email).to eq(downcased_email)
      end
    end
  end

  describe '#authenticate' do
    let(:incorrect_password) { 'incorrect' }

    context 'with correct password' do
      it 'returns the user' do
        expect(user.authenticate(password)).to be(user)
      end
    end

    context 'with incorrect password' do
      it 'returns false' do
        expect(user.authenticate(incorrect_password)).to be false
      end
    end
  end
end
