# frozen_string_literal: true

# Create the users table.
#
# Users have an email and password. Users are authenticated with Rails' built-in
# `has_secure_password`. Passwords are salted and hashed using BCrypt. The salt
# and hash are stored in the `password_digest` column.
class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest

      t.timestamps
    end
  end
end
